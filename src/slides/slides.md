# DRUPAL DECOUPLED WITH GATSBY 
### INTRODUCTION

---

## WHAT IS GATSBY
- static site generator
- open source
- based on React

---

## HOW GATSBY WORKS
![](./how-gatsby-works.png)

---

## GATSBY + DRUPAL
![](./gatsby-and-drupal.png)

---
## KNOWLEDGE
- Drupal 8:
  - create content
  - enable modules
- Gatsby:
  - basics React (classes + hooks)
  - NodeJS + NPM
  
---

## GATSBY TOOLS
- gatsby-cli
- starters
- plugins 
- external resources (React)

---

## GATSBY CLI
- npm install -g gatsby-cli
- gatsby --help
- gatsby new [rootPath] [starter]

     example:
    
    gatsby new my-first-gatsby-project https://github.com/gatsbyjs/gatsby-starter-hello-world

---

## PLUGINS FOR DRUPAL
- gatsby-source-plugin 
- gatsby-drupal-webform
- gatsby-remark-drupal
- gatsby-theme-drupal
- gatsby-plugin-drupal-comments
- gatsby-source-drupal-multilanguage
- gatsby-source-drupal7

---

## PROJECT STRUCTURE
    |-- /.cache 
    |-- /node_modules
    |-- /public
    |-- /src
        |-- /components
        |-- /pages
        |-- /templates
        |-- html.js
    |-- /static
    |-- gatsby-config.js
    |-- gatsby-node.js
    |-- gatsby-ssr.js
    |-- gatsby-browser.js

---

## IMAGES
- Gatsby Image API
- gatsby-image = React Component
- optimize image loading 
- Demo: https://using-gatsby-image.gatsbyjs.org/

---

## DEPLOYING GATSBY WEBSITE
- gatsby build
- upload directory: public

---

## Recap Drupal Decoupled with Gatsby
- create an API server = Drupal 8 
   - create content
   - module: jsonAPI
- create a Gatsby site
  - plugin gatsby-source-drupal
  - build a GraphQL query
  - create a page 
- Deploy Gatsby site


---


## CREATE PRESENTATIONS WITH GATSBY

Gatsby Deck
- github:  https://github.com/fabe/gatsby-starter-deck
- gatsby-cli:  gatsby new my-slides https://github.com/fabe/gatsby-starter-deck
content slides = markdown-Files
- Demo: https://gatsby-deck.netlify.com/1

---

## LINKS
- https://www.gatsbyjs.org/
- https://reactjs.org
- https://www.getpostman.com
- https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-source-drupal
- https://github.com/fabe/gatsby-starter-deck
- https://www.lullabot.com/articles/decoupled-drupal-getting-started-gatsby-and-jsonapi

---

## Showtime

---

## THANK YOU!