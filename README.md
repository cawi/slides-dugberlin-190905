# DRUPAL DECOUPLED WITH GATSBY (Introduction)

Talk about Drupal and Gatsby on 5th september 2019 in Berlin at Drupal User Group. 

All slides are created with Gatsby Deck. 


[Demo](https://cawi.gitlab.io/slides-dugberlin-190905/1)

## Credits

Fabian Schultz (@fschultz_)

Gatsy Deck https://github.com/fabe/gatsby-starter-deck
